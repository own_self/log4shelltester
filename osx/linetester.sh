#!/bin/sh

a=($(wc -c bak.out.log))
bytes=${a[0]}
echo "$bytes"

if [ $bytes -ge 0 ]
then
  echo "vulnerable"
else
  echo "safe"
fi
