# README #

### What is this repository for? ###

* log4shell Vulnerability tester for Web Applications

### How do I get set up? ###

* download all the files into local
* Command - ./vulntester.sh POST localhost 8080 'behaviour/test' '${jndi:ldap://127.0.0.1:8888/blah}'
* replace <behaviour/test> with the path to your application's URL
* replace port with application's port
* Make sure that this URL's handler controller/service Logs the request body as a whole using Log4j2 library
* Make sure that system running the script and application must have ncat (nc) installed
* Do run the script 4-5 times for consistent results